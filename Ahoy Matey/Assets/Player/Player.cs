﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Networking;

public class Player : NetworkBehaviour
{
    private Vector3 translateValue;
    private Vector3 rotateValue;
    private float currentRotation = 0f;

    // Use this for initialization
    void Start ()
    {
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!isLocalPlayer) { return; }

        translateValue.x = 0f;
        translateValue.y = 0f;
        translateValue.z = CrossPlatformInputManager.GetAxis("Vertical") / 5f;

        transform.Translate(translateValue);
        transform.position = new Vector3 (Mathf.Clamp(transform.position.x , -500f , 500f) , 0f , Mathf.Clamp(transform.position.z, -500f, 500f));

        rotateValue.x = 0;
        rotateValue.y = CrossPlatformInputManager.GetAxis("Horizontal");
        rotateValue.z = 0;

        transform.Rotate(rotateValue);
    }

    public override void OnStartLocalPlayer()
    {
        GetComponentInChildren<Camera>().enabled = true;
    }
}
